const cacheName = "v1";

const cacheAssets = [
    'index.html',
    './admin/index.html',
    './login/index.html',
    './resetpassword/index.html',
    './css/main.css',
    './js/main.js',
    './js/admin.js',
    './js/reset.js',
    './js/isMobile.js',
    './js/jquery-ui-1.12.1.custom/jquery-ui.min.js',
    './js/jquery-3.4.1.min.js',
    './js/hammer.min.js',
    './js/jquery.hammer.js',
    './js/jquery.ui.touch-punch.min.js',
    '/img/essentialresources/arrow.png',
    '/img/essentialresources/cross.png',
    '/img/essentialresources/userPosition.png',
    '/img/essentialresources/swipe.svg',
    '/img/essentialresources/header.svg',
    '/img/essentialresources/apple-touch-icon-152x152.png',
    '/img/essentialresources/mountain.svg',
    '/img/essentialresources/mail.svg',
    '/img/essentialresources/logo.svg',
    '/img/essentialresources/locate.svg',
    '/img/essentialresources/info.svg',
    '/img/essentialresources/warning.svg'
];

self.addEventListener('install', (e) => {
    e.waitUntil(
        caches.open(cacheName).then(cache => {
            cache.addAll(cacheAssets);
        })
            .then(() =>{
                self.skipWaiting();
            })
    );
});

self.addEventListener('activate', (e) => {
    e.waitUntil(
    caches.keys().then(cacheNames =>{
        return Promise.all(
            cacheNames.map(cache =>{
                if(cache !== cacheName){
                    return caches.delete(cache);
                }
            })
        )
    })
    );
});

self.addEventListener('fetch',e =>{
    e.respondWith(fetch(e.request).catch(()=>{
        caches.match(e.request);
    }));
});
