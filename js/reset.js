let reset = (function () {
    let cookie = document.cookie;
    function init() {
        if (cookie) {
            if (cookie.indexOf("reset") < 0) {
                window.location.href = "../login";
            }
        }
        if ('serviceWorker' in navigator) {
            $(window).on("load", async () => {
                try {
                    await navigator.serviceWorker
                        .register("../serviceworker.js");
                }
                catch (ex) {
                    console.log(ex);
                }
            });
        }
    }
    $(".passwordResetField").on("submit", function (e) {
        e.preventDefault();
        changePassword();
    });
    function changePassword(){
        if($("#password").val() != $("#password2").val()){
            $(".status").html("Error: The passwords don't match");
            return false;
        }
        let cook,
        value = "; " + cookie,
        parts = value.split("; reset=");
        if (parts.length == 2) cook = parts.pop().split(";").shift();
        fetch('../api/users/resetpassword', {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-reset-token': cook
            },
            body: JSON.stringify({
                newPassword: $("#password").val(),
                repeatPassword: $("#password2").val()
            })
        })
        .then((res) => {
            if (res.status >= 200 && res.status < 300) {
                return res.json()
            }
            throw (res);
        })
        .then((data) => {
            $(".status").html(data.msg);
            document.cookie = "reset=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
            setTimeout(function () {
                window.location.href = "../login";
            }, 3500)
        }).catch((err) => {
            if (err.status < 500) {
                err.json().then((data) => {
                    $(".status").html(data.msg);
                })
            }
            else {
                $(".status").html(err.statusText);
            }
        });
    }
    return {
        init: init
    };
})();

reset.init();