const MapApp = (function() {
    const original = [46.579000, 11.8721],
        kartan = L.map('map');
    let currentPos = original,
        i = 0,
        suffix = [],
        myPos, resizeCD = !1,
        tiles, errorLoading = !1,
        coolDown = !1,
        tileErrorCD = !1,
        MK = [],
        distanceBetweenPoints = [],
        places, iv, ww = window.innerWidth,
        wh = window.innerHeight,
        cook, value = "; " + document.cookie,
        parts = value.split("; id=");
    async function init() {
        if (parts.length == 2) cook = parts.pop().split(";").shift();
        initMapAssets();
        $(".expanded").hammer({
            touchAction: 'auto'
        }).data('hammer').get('swipe')
        $(".swipeContainer").hammer().data('hammer').get('swipe').set({
            direction: Hammer.DIRECTION_ALL
        });
        $(".messageBox").hammer().data('hammer').get('swipe').set({
            direction: Hammer.DIRECTION_ALL
        });
        clearFields();
        $("#map").addClass("small");
        setTimeout(function() {
            $("#map").removeClass("small")
        }, 60);
        if ('serviceWorker' in navigator) {
            $(window).on("load", async () => {
                try {
                    await navigator.serviceWorker.register("serviceworker.js")
                } catch (ex) {
                    errorDisplay(ex);
                    setTimeout(function() {
                        resetError()
                    }, 5000)
                }
            })
        }
        async function initMapAssets() {
            if ($("#errorStatus").text().indexOf("seconds") > -1) {
                clearInterval(iv)
            }
            try {
                const ready = await initializeMap();
                if (ready) {
                    regularUpdate();
                    addMarkers();
                    resetError()
                }
            } catch (ex) {
                errorDisplay(`${ex}, retrying in <span class='counter'>5</span> seconds`);
                iv = setInterval(function() {
                    let seconds = parseInt($(".counter").text());
                    seconds--;
                    $(".counter").text(seconds)
                }, 1000);
                setTimeout(function() {
                    initMapAssets()
                }, 5000)
            }
        }
    }

    function initializeMap() {
        return new Promise(resolve => {
            try {
                tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic2FkZGV4IiwiYSI6ImNqb2lqbmtkNDA4em8zcW1uczA0Nzc0ZGYifQ.w0F2_Yj243eE69C_o8jNHQ', {
                    attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
                    tileSize: 512,
                    maxZoom: 18,
                    zoomOffset: -1,
                    id: 'mapbox/streets-v11',
                }).addTo(kartan)
            } catch (ex) {
                reject()
            }
            kartan.setView(currentPos, 10);
            kartan.locate({
                setView: !1,
                maxZoom: 18
            });
            kartan.on('locationfound', PosSuccess);
            L.control.scale().addTo(kartan);
            setTimeout(function() {
                kartan.invalidateSize()
            }, 250);
            resolve(!0)
        })
    }
    $("#logo").on("mousedown", function(e) {
        if (e.button == 0) {
            window.location.reload()
        }
    });
    $("#mail").on("mousedown", function(e) {
        e.stopPropagation();
        if (e.button == 0) {
            if ($(".messageBox").hasClass("enabled") == !1) {
                $(".messageBox").addClass("enabled");
                closeBox("info")
            } else {
                closeBox("message")
            }
        }
    });
    $("#info").on("mousedown", function(e) {
        e.stopPropagation();
        if (e.button == 0) {
            if ($(".infoBox").hasClass("enabled") == !1) {
                $(".infoBox").addClass("enabled");
                closeBox("message")
            } else {
                closeBox("info")
            }
        }
    });
    kartan.on('tileerror', function() {
        if (tileErrorCD == !1) {
            tileErrorCD = !0;
            tiles.redraw();
            setTimeout(function() {
                tileErrorCD = !1
            }, 3000)
        }
    });
    $(".mailForm, .bubble").on("mousedown", function(e) {
        e.stopPropagation()
    });
    $(".mailForm").submit(function(e) {
        e.preventDefault();
        let email = $(".mail").val();
        let subject = $(".subject").val();
        let content = $(".content").val();
        let captcharesponse = $(".g-recaptcha-response").val();
        if (email != null && subject != null && content != null && subject != "" && content != "" && email != "" && email != undefined && subject != undefined && content != undefined) {
            let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == !1) {
                $(".status").html("Invalid email adress format")
            } else if (subject.length > 120) {
                $(".status").html("Subject can't be longer than 120 characters")
            } else if (content.length > 10000) {
                $(".status").html("Too long message, max 10 000 characters is allowed")
            } else {
                sendReq("post", "", "send", {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }, "", JSON.stringify({
                    email: email,
                    subject: subject,
                    content: content,
                    captcha: captcharesponse
                })).then(function() {
                    $(".status").addClass("green");
                    $(window).off("beforeunload");
                    $(".status").html("Message successfully sent!");
                    setTimeout(function() {
                        $(".messageBox").removeClass("enabled");
                        $(".status").removeClass("green").html("");
                        clearFields()
                    }, 800)
                }).catch(function(err) {
                    $(".status").html(err)
                })
            }
        } else {
            $(".status").html("One or more fields have been left empty")
        }
    });
    kartan.on('popupopen', async function() {
        let fullId = ($(".leaflet-popup-content").find(".thumb").attr("id"));
        let _id = fullId.split('-')[0];
        let j = fullId.split('-')[1];
        if (!places[j].rating) {
            let retrievedScore = await requestScore(_id);
            places[j].rating = retrievedScore;
            loadScore(j, retrievedScore)
        } else {
            loadScore(j, places[j].rating)
        }
        $(".fa-star").on("mousedown", function(e) {
            if (e.button == 0 && $(this).parent().parent().hasClass("leaflet-popup-content")) {
                let id = $(".leaflet-popup-content").find(".thumb").attr("id");
                let j = id.split('-')[1];
                id = id.split('-')[0];
                let name = $("#placeTitle").text();
                let score = $(e.originalEvent.target).attr('id').split('-')[1];
                if (score == places[j].rating) {
                    deleteRating(id, j)
                } else {
                    sendRating(id, score, j, name)
                }
            }
        });
        $(".fa-star").on("mouseover", function(e) {
            let id = $(e.originalEvent.target).parent().attr('id');
            id = id.split('-')[1];
            let score = $(e.originalEvent.target).attr('id').split('-')[1];
            loadScore(id, score)
        }).on("mouseout", function(e) {
            let id = $(e.originalEvent.target).parent().attr('id');
            id = id.split('-')[1];
            $("#rating-" + id).children().removeClass("checked");
            if (places[id].rating != null) {
                loadScore(id, places[id].rating)
            }
        });
        $(".thumb").on("mousedown", function(e) {
            if (e.button == 0) {
                $(".imagePopup").addClass("enabled");
                let id = $(e.originalEvent.target).attr('id');
                id = id.split('-')[1];
                loadImg(id)
            }
        })
    });
    $(document).on("mousedown", function(e) {
        if (e.button == 0) closeBox("both")
    });
    $(document).on("mousedown", ".imagePopup", function(e) {
        if ($(".imagePopup").hasClass("enabled") == !0 && ww > 900 && e.button == 0) {
            closeWindow()
        }
    });
    $(document).on("mousedown", ".cross", function(e) {
        if ($(".imagePopup").hasClass("enabled") && e.button == 0) {
            closeWindow()
        }
    });
    $(document).on("mousedown", ".arrow", function(e) {
        e.stopPropagation();
        if (e.button == 0) {
            let j = $(".expanded").attr("id").split('-')[1];
            if ($(e.originalEvent.target).is("#nr1")) {
                if (j > 0) {
                    j--
                } else {
                    j = places.length - 1
                }
            } else {
                if (j == places.length - 1) {
                    j = 0
                } else {
                    j++
                }
            }
            loadImg(j)
        }
    });
    $(document).on("mousedown", ".desc, .expanded", function(e) {
        e.stopPropagation()
    });
    $(document).on("mousedown", ".goTo", function(e) {
        e.stopPropagation();
        if (e.button == 0) {
            let id = $(e.originalEvent.target).attr('id').split('-')[1];
            kartan.setView(places[id].pos.coordinates, 10);
            kartan.closePopup();
            MK[id].openPopup();
            closeWindow()
        }
    });
    $(document).on("mousedown", ".fa-star", function(e) {
        e.stopPropagation();
        if (e.button == 0) {
            let id = $(e.originalEvent.target).parent().parent().parent().find(".expanded").attr('id');
            let j = id.split('-')[1];
            id = id.split('-')[0];
            let score = $(e.originalEvent.target).attr('id').split('-')[1];
            let name = $(".popTitle").text();
            if (score == places[j].rating) {
                deleteRating(id, j)
            } else {
                sendRating(id, score, j, name)
            }
        }
    });
    $(document).on("mouseover", ".fa-star", function(e) {
        let id = $(e.originalEvent.target).parent().attr('id');
        id = id.split('-')[1];
        let score = $(e.originalEvent.target).attr('id').split('-')[1];
        loadScore(id, score)
    }).on("mouseout", ".fa-star", function(e) {
        let id = $(e.originalEvent.target).parent().attr('id');
        id = id.split('-')[1];
        $("#rate2-" + id).children().removeClass("checked");
        if (places[id].rating != null) {
            loadScore(id, places[id].rating)
        }
    });
    $(".goToButton").on("mousedown", function(e) {
        if (e.button == 0) {
            if ($(e.originalEvent.target).attr("id") == "locate") {
                kartan.locate({
                    setView: !0,
                    maxZoom: 16
                });
                kartan.on('locationfound', PosSuccess)
            } else {
                kartan.setView(original, 10)
            }
        }
    });
    $(".closeForm").on("mousedown", function(e) {
        if (e.button == 0) {
            if ($(".messageBox").hasClass("enabled")) {
                closeBox("message")
            } else if ($(".infoBox").hasClass("enabled")) {
                closeBox("info")
            }
        }
    });
    $(".expanded, .desc").hammer().bind("swipeleft", function(e) {
        if (isMobile.any == !0) {
            let j = $(".expanded").attr("id").split('-')[1];
            if (j == places.length - 1) {
                j = 0
            } else {
                j++
            }
            loadImg(j)
        }
    });
    $(".expanded, .desc").hammer().bind("swiperight", function(e) {
        if (isMobile.any == !0) {
            let j = $(".expanded").attr("id").split('-')[1];
            if (j > 0) {
                j--
            } else {
                j = places.length - 1
            }
            loadImg(j)
        }
    });
    $(".bubble, .infoWrap, form").hammer({
        domEvents: !0
    }).bind("swiperight", function(e) {
        if (isMobile.any == !0) {
            $(".bubble").css("transform", "translateX(2000px)");
            setTimeout(function() {
                closeBox("both");
                $(".bubble").css("transform", "translateX(0px)")
            }, 300)
        }
    });
    $(".mailForm").on("keyup", function(e) {
        if ($(".mail").val().length > 0 || $(".subject").val().length > 0 || $(".content").val().length > 0) {
            $(window).on("beforeunload", function() {
                return "You have not sent your message. All changes will be lost"
            })
        } else {
            $(window).off("beforeunload");
        }
    });
    async function deleteRating(_id, j) {
        getCookie();
        try {
            await sendReq("delete", "", "reviews/", {
                "x-user": cook,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }, _id, null);
            places[j].rating = 0;
            loadScore(j, 0)
        } catch (err) {
            errorDisplay(err);
            setTimeout(function() {
                resetError()
            }, 5000)
        }
    }
    async function sendRating(_id, score, j, name) {
        getCookie();
        try {
            const data = await sendReq("post", "", "reviews", {
                "x-user": cook,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }, "", JSON.stringify({
                user: cook,
                score: score,
                location: {
                    title: name,
                    _id: _id
                }
            }));
            places[j].rating = data.score;
            loadScore(j, data.score)
        } catch (err) {
            errorDisplay(err);
            setTimeout(function() {
                resetError()
            }, 5000)
        }
    }

    function requestScore(_id) {
        getCookie();
        return new Promise(async (resolve, reject) => {
            try {
                const data = await sendReq("get", "", "reviews/", {
                    "x-user": cook
                }, _id, null);
                return resolve(data.score)
            } catch (err) {
                resolve(0)
            }
        })
    }

    function closeBox(id) {
        if (id == "both") {
            $(".messageBox,.infoBox").removeClass("enabled")
        } else {
            if ($("." + id + "Box").hasClass("enabled")) $("." + id + "Box").removeClass("enabled")
        }
    }

    function closeWindow() {
        $(".imagePopup").removeClass("enabled")
    }
    async function loadImg(id) {
        $(".expanded").attr("id", places[id]._id + "-" + id + "-").attr("src", "/img/" + places[id].img);
        $(".popTitle").text(places[id].title);
        $(".popDesc").text(places[id].desc);
        $(".altSpan").text(places[id].alt);
        $(".rate2").attr("id", "rate2-" + id);
        getPoints(id);
        $(".dist2").text(distanceBetweenPoints[id] + suffix[id]).attr("id", "dist2-" + id);
        $(".goTo").attr("id", "goTo-" + id);
        $(".desc,.cross").css("display", "none");
        let _id = $(".expanded").attr("id").split('-')[0];
        const data = await requestScore(_id);
        places[id].rating = data;
        loadScore(id, places[id].rating)
    }

    function middleWare(success) {
        let id = $(".expanded").attr("id").split('-')[1];
        if (success == !0) {
            errorLoading = !1;
            setSize(id)
        } else {
            errorLoading = !0;
            if (coolDown == !1) {
                coolDown = !0;
                if (places) $(".expanded").attr("src", "/img/" + places[id].img + '?' + Math.random());
                setTimeout(function() {
                    coolDown = !1
                }, 5000)
            }
            if (errorLoading == !0) {
                setTimeout(function() {
                    middleWare(!1)
                }, 5000)
            }
        }
    }

    function clearFields() {
        $(".mail,.subject,.content").val("")
    }
    $(window).bind("resize", function() {
        if (resizeCD == !1) {
            resizeCD = !0;
            ww = window.innerWidth;
            wh = window.innerHeight;
            if ($(".imagePopup").hasClass("enabled") == !0) {
                let id = $(".expanded").attr("id").split('-')[1];
                setSize(id)
            }
            setTimeout(function() {
                resizeCD = !1
            }, 25)
        }
    });

    function setSize(id) {
        if ($(".imagePopup").hasClass("enabled") == !0) {
            let eW = $(".expanded").width();
            if (places[id].hgw == !1 && (ww / wh) > 0.9 && ww > 900 && eW > ww) {
                $(".desc").removeClass("auto").css("width", eW - 16).css("margin-left", ((ww - eW) / 2) - 8);
                $(".expanded").removeClass("high");
                crossSize(eW)
            } else if (places[id].hgw == !1 && (ww / wh) > 0.9 && ww <= 900 && eW > ww) {
                $(".desc").removeClass("auto");
                $(".expanded").removeClass("high");
                crossSize(eW + 120)
            } else if (places[id].hgw == !0 && (ww / wh) > 0.5 && ww <= 900 && eW > ww) {
                $(".desc").removeClass("auto");
                $(".expanded").removeClass("high");
                $(".cross").css("display", "block")
            } else {
                $(".expanded").addClass("high");
                $(".desc").addClass("auto");
                $(".cross").css("display", "block")
            }
            $(".desc").css("display", "block");

            function crossSize(eW) {
                $(".cross").css("margin-left", ((ww - eW) / 2) - 50 + eW).css("display", "block")
            }
        }
    }

    function PosSuccess(e) {
        currentPos = [e.latlng.lat, e.latlng.lng];
        let minPositionIkon = L.icon({
            iconUrl: 'img/essentialresources/userPosition.png',
            iconSize: [32, 32]
        });
        if (!myPos) {
            myPos = L.marker(e.latlng, {
                icon: minPositionIkon
            }).addTo(kartan)
        }
        myPos.setLatLng(currentPos).update()
    }

    function sendReq(method, query, dest, headers, id, body) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await fetch(`/api/${dest}${id}${query}`, {
                    method: method,
                    mode: 'cors',
                    headers: headers,
                    body: body
                });
                if (res.status >= 200 && res.status < 400) {
                    const contentType = res.headers.get("content-type");
                    if (contentType && contentType.indexOf("application/json") !== -1) {
                        return resolve(res.json())
                    } else {
                        return resolve(res.text())
                    }
                } else {
                    throw (res)
                }
            } catch (err) {
                const c = await convertError(err);
                reject(c)
            }
        })
    }
    async function addMarkers() {
        if ($("#errorStatus").text().indexOf("seconds") > -1) {
            clearInterval(iv)
        }
        try {
            const data = await sendReq('get', 'locations', "", {
                "x-user": cook
            }, "", null);
            let d = new Date();
            d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
            let expires = "expires=" + d.toUTCString();
            document.cookie = "id =" + data.id + ";" + expires + ";path=/";
            places = data.data;
            suffix = [places.length];
            distanceBetweenPoints = [places.length];
            for (i = 0; i < places.length; i++) {
                if (!places[i].score) places[i].score = "Not rated yet";
                MK[i] = L.marker(places[i].pos.coordinates);
                MK[i].options.icon.id = places[i]._id;
                MK[i].addTo(kartan);
                getPoints(i);
                setPopup()
            }
            resetError()
        } catch (ex) {
            errorDisplay(`${ex}, retrying in <span class='counter'>5</span> seconds`);
            iv = setInterval(function() {
                let seconds = parseInt($(".counter").text());
                seconds--;
                $(".counter").text(seconds)
            }, 1000);
            setTimeout(function() {
                addMarkers()
            }, 5000)
        }
    }

    function resetError() {
        $("#errorBar").removeClass("active");
        $("#errorStatus").html("")
    }

    function errorDisplay(err) {
        $("#errorBar").addClass("active");
        $("#errorStatus").html("<img src='/img/essentialresources/warning.svg'> Error: " + err)
    }

    function convertError(err) {
        return new Promise((resolve, reject) => {
            let extracted = "";
            if (err.statusText != undefined) {
                err.json().then(data => {
                    extracted = data.msg;
                    return resolve(extracted)
                }).catch(ex => {
                    extracted = err.statusText;
                    return resolve(extracted)
                })
            } else {
                return resolve("No connection, or the operation was aborted")
            }
        })
    }

    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        let R = 6371;
        let dLat = degTorad(lat2 - lat1);
        let dLon = degTorad(lon2 - lon1);
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(degTorad(lat1)) * Math.cos(degTorad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        if (d < 1) {
            d = Math.floor(d * 1000);
            suffix[i] = " m"
        } else {
            d = (Math.round(d * 10)) / 10;
            if (d > 100) {
                d = Math.round(d)
            }
            suffix[i] = " km"
        }
        return d
    }

    function getPoints(id) {
        distanceBetweenPoints[id] = getDistanceFromLatLonInKm(places[id].pos.coordinates[0], places[id].pos.coordinates[1], currentPos[0], currentPos[1])
    }

    function getCookie() {
        value = "; " + document.cookie;
        parts = value.split("; id=");
        if (parts.length == 2) cook = parts.pop().split(";").shift()
    }

    function loadScore(id, object) {
        $("#rating-" + id).children().removeClass("checked");
        $(".rate2").children().removeClass("checked");
        for (let j = 1; j <= object; j++) {
            $("#rating-" + id).find("#star-" + j).addClass("checked");
            $(".rate2").find("#star2-" + j).addClass("checked")
        }
    }

    function setPopup() {
        let template = " src='/img/" + places[i].img + "' title='Click to enlarge' id='" + places[i]._id + "-" + i + "'><h3 id='placeTitle'>" + places[i].title + "</h3><p>" + places[i].desc + "</p><h3>Altitude: <span>" + places[i].alt + " m above sea level</span></h3>" + "<h3>Distance: <span id='dist-" + i + "'>" + distanceBetweenPoints[i] + suffix[i] + "</span> away</h3>" + "<h3>Average rating: " + places[i].score + "</h3>" + "<div class='rate' id='rating-" + i + "'><span id='star-1' class='fa fa-star' title='Very bad'></span>" + "<span id='star-2' class='fa fa-star' title='Bad'></span>" + "<span id='star-3' class='fa fa-star' title='Average'></span>" + "<span id='star-4' class='fa fa-star' title='Good'></span>" + "<span id='star-5' class='fa fa-star' title='Very good'></span></div>";
        if (places[i].hgw == !0) {
            MK[i].bindPopup("<img class='thumb high'" + template)
        } else {
            MK[i].bindPopup("<img class='thumb'" + template)
        }
    }

    function degTorad(deg) {
        return deg * (Math.PI / 180)
    }

    function regularUpdate() {
        window.setInterval(function() {
            if (places) {
                kartan.locate({
                    setView: !1
                });
                kartan.on('locationfound', PosSuccess);
                for (i = 0; i < places.length; i++) {
                    getPoints(i);
                    setPopup();
                    $("#dist-" + i).html(distanceBetweenPoints[i] + suffix[i]);
                    $(".dist2-" + i).html(distanceBetweenPoints[i] + suffix[i])
                }
            }
        }, 4000)
    }
    return {
        init: init,
        middleWare: middleWare
    }
})();
MapApp.init()