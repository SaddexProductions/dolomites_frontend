let admin = (function () {
    let cookie,
        hovered = [false, false],
        hovered2 = [false, false],
        selected = [],
        user_id,
        chosen = true,
        firstResultImage,
        site = 0,
        first = [true, true],
        elem,
        signal,
        controller,
        objectList,
        cook,
        replacedFile,
        ww,
        wh,
        setNew = false,
        sub = 90;
    const te = "<div class='te'><i class='fas fa-edit'></i><img class='trash' src='../img/essentialresources/trashcan.svg' loading='lazy'></div>",
        addP = ["locations", "reviews", "images"];
    async function init() {
        hasToken = retrieveCookie();
        if (hasToken) {
            try {
                const res = await sendReq('get', '', 'login', {
                    'x-auth-token': cook,
                    'x-no': true
                }, "", null);
                user_id = res._id;
                generateContent();
                setSize();
            }
            catch (ex) {
                window.location.href = "../login";
            }
        }
        else {
            window.location.href = "../login";
        }
        if (isMobile.any) {
            $(".leftBar, .rightBar, .dbObjs").addClass("softNav");
        }
        if ('serviceWorker' in navigator) {
            $(window).on("load", async () => {
                try {
                    await navigator.serviceWorker
                        .register("../serviceworker.js");
                }
                catch (ex) {
                    errorPrompt(ex);
                }
            });
        }
    }
    function retrieveCookie() {
        cookie = document.cookie;
        if (cookie && cookie.indexOf("token") > -1) {
            let value = "; " + cookie;
            let parts = value.split("; token=");
            if (parts.length == 2) cook = parts.pop().split(";").shift();
            return true;
        }
        return false;
    }
    function expandReviewsList() {
        if ($(".control").height() > 117) {
            $(".extendedReviews").addClass("ext");
        }
        else {
            $(".extendedReviews").removeClass("ext");
        }
    }
    async function insertIntoList(id, query, dest) {
        $(".oL, .overLay").remove();
        let proceed = false,
            additionalClasses = "",
            add2 = "";
        $(".insReviews").removeClass("active");
        if (site == 0) {
            try {
                objectList = await sendReq('get', query, dest, { 'x-no': true }, "", null);
                proceed = true;
                objectList = objectList.data;
            }
            catch (err) {
                const c = await convertError(err);
                errorPrompt(c);
                noItem();
            }
        }
        else if (site == 1) {
            additionalClasses = " active";
            add2 = " compact";
            try {
                objectList = await sendReq('get', query, `reviews/${dest}`, { 'x-auth-token': cook }, `/${id}`, null);
                proceed = true;
            }
            catch (err) {
                let c = await convertError(err);
                errorPrompt(c);
                noItem();
            }
        }
        else {
            try {
                objectList = await sendReq('get', query, 'images', { 'x-auth-token': cook }, "", null);
                proceed = true;
            }
            catch (err) {
                let c = await convertError(err);
                errorPrompt(c);
                noItem();
            }
        }
        if (proceed) {
            for (let i = 0; i < objectList.length; i++) {
                if (!objectList[i].score) objectList[i].score = "Not rated yet";
                const result = inject(objectList[i]);
                $(".dbObjs").append(`<div id='${objectList[i]._id}' class='oL${add2}'></div><div class='overLay ${objectList[i]._id} ${additionalClasses}'></div>`);
                $(`#${objectList[i]._id}`).html(result);
                if (site == 1) {
                    $(".fa-edit").remove()
                }
            }
            checkIfScrollButtonsNeeded($(".dbObjs"));
            return;
        }
    }
    function noItem() {
        $(".insReviews").addClass("active").text(`There are no ${addP[site]} matching the above query.`);
    }
    function errorPrompt(err) {
        $("#errorDesc").html(err);
        $("#errorPrompt").dialog({
            resizable: false,
            height: "auto",
            width: 340,
            modal: true,
            buttons: {
                "Okay": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    async function generateContent() {
        $(".admin").append("<header><a href='../'><img class='loginLogo' src='../img/essentialresources/apple-touch-icon-152x152.png'></a><ul>" +
            "<li><a href='#locations' id='link-0' class='link'>Locations</a></li><li id='store'><a href='#reviews' id='link-1' class='link'>Reviews</a></li>" +
            "<li><a href='#images' class='link' id='link-2'>Images</a></li></ul>" +
            "<a class='link' id='link-3'><i class='fas fa-user' id='" + user_id + "'></i></a></header><div class='userMenu disabled'><ul>" +
            "<li class='changePw'>Change password <i class='fas fa-user-edit'></i></li>" +
            "<li class='logout'>Logout <i class='fas fa-sign-out-alt'></i></li></ul></div><div class='passwordBox bubble'>" +
            "<div class='arr top right white'></div><div class='arr top right black'></div><img class='closeForm' src='../img/essentialresources/cross.png'>" +
            "<form class='pwForm'><h1>Change password</h1><h3 id='fL'>Current password</h3><input type='password' id='oldPw' placeholder='(Required)' required>" +
            "<h3>New password</h3><input type='password' id='newPw' placeholder='Must be at least 5 characters long' required>" +
            "<h3>Repeat new password</h3><input type='password' id='repeatNewPw' placeholder='Type the new password once more' required>" +
            "<input type='submit' id='submit3' value='Submit'><p id='statusMessagePassword' class='statusM'></p></form></div><div class='imagePopup'>" +
            "<img class='cross' src='../img/essentialresources/cross.png' title='Close'><img class='popupPreview' onload=admin.measure()></div><div class='leftBar'>" +
            "<div class='control'><input id='searchField' placeholder='Search by title...' type='text'></input><div class='autoComplete'></div>" +
            "<button class='controlButton advanced' title='Advanced search options'>Advanced...</button>" +
            "<div class='filterHolder'><input type='number' step='0.1' class='ratingsFilter sN4 rT dI' data-index='0' value='1'></input>" +
            "<div id='slider4Container'><div id='slider4'>" +
            "<div class='left-color'></div></div></div><input type='number' step='0.1' class='ratingsFilter sN4 rT dI' id='rF2' data-index='1' value='5'></input></div>" +
            "<button class='controlButton searchButton' title='Search for items'><i class='fas fa-search'></i></button>" +
            "<div class='cS'><p class='confirmedSearch-1'><span></span>" +
            "<img id='cross' class='cross2' src='../img/essentialresources/cross.png'></p><p class='confirmedSearch-2'><span></span>" +
            "<img class='cross2' id='cross2' src='../img/essentialresources/cross.png'></p></div><div class='buttonContainer'>" +
            "<button class='controlButton plus' title='Add new location'>+</button></div><div class='moreOptions' id='mO0'>" +
            "<div class='oF'><div class='aC up'><i class='fas fa-arrow-circle-up'></i></div><div class='aC down'><i class='fas fa-arrow-circle-down'></i></div></div>" +
            "<div class='conT'><h3>Description: </h3><textarea id='descFilter' placeholder='Filter by description...' maxlength='500'></textarea>" +
            "</div><div class='conT' id='contAlt'>" +
            "<h3>Altitude range (meters): </h3><div class='sliderHolder'><input type='number' class='sN1 smallNum dI' data-index='0' value='0' max='4000' min='0'/>" +
            "<input type='number' class='sN1 smallNum dI' data-index='1' value='4000' id='field2'/><div id='slider1'><div class='left-color'></div></div></div>" +
            "<h3>Average rating: </h3><input type='number' class='sN3 smallNum rT dI' data-index='0' value='1' step='0.1'/>" +
            "<input type='number' class='sN3 smallNum rT dI' data-index='1' value='5' id='ratingsField2' step='0.1'/>" +
            "<div id='slider3'><div class='left-color'></div></div></div>" +
            "<div class='conT' id='coorImg'><h3>Image:</h3><input type='text' placeholder=' Image name' id='imgName'></input>" +
            "<div class='selectPanel'><label><input id='use' type='checkbox'>Filter by image height-width</label>" +
            "<label class='disabled' id='toggleLabel'><input id='hwRatio' type='checkbox' disabled>Height greater than width</label></div></div>" +
            "<div class='conT' id='coorSearch'><h3>Coordinates: </h3><input class='coor-search' type='number' step='0.0000001' id='la-search'>" +
            "<input class='coor-search' type='number' step='0.0000001' id='lo-search'><h4>Distance range (km):</h4>" +
            "<input type='number' id='searchRange' value='5' min='1' max='150'><div id='slider2'></div></div>" +
            "</div><div class='moreOptions' id='mO2'>" +
            "<div class='oF two'><div class='aC up'><i class='fas fa-arrow-circle-up'></i></div><div class='aC down'><i class='fas fa-arrow-circle-down'></i></div></div>" +
            "<div class='conT'><h3>Extension</h3><input id='extSearch' type='test' placeholder='JPG, PNG, etc...'></div><div class='conT' id='conTsize'><h3>Size (kb)</h3>" +
            "<input type='number' min='0' max='4000' id='minSize' value='0' class='sN5 sR dI' data-index='0'>" +
            "<input type='number' min='0' max='4000' id='maxSize' value='4000' class='sN5 sR dI' data-index='1'>" +
            "<div id='slider5'><div class='left-color'></div></div></div><div class='conT' id='conTused'>" +
            "<h3>Used by</h3><textarea id='usedByFilter' maxlength='100' placeholder='Multiple locations can be specified, separated by commas...'></textarea></div></div></div>" +
            "<p class='insReviews'>Search reviews by searching for locations (or through the 'locations' tab) or users above. User queries must start with &quot;User:&quot;</p>" +
            "<div class='dbObjs'>" +
            "<div class='oF'><div class='aC up'><i class='fas fa-arrow-circle-up'></i></div><div class='aC down'><i class='fas fa-arrow-circle-down'></i></div></div>" +
            "</div></div><div class='rightBar'><img class='closeWindow cross' loading='lazy' src='../img/essentialresources/cross.png'>" +
            "<div class='oF'><div class='aC up'><i class='fas fa-arrow-circle-up'></i></div><div class='aC down'><i class='fas fa-arrow-circle-down'></i></div></div>" +
            "<p class='noItems'>Click on an item to bring up the editor, or create a new one</p><form class='edit0 editForm'><h4>Title</h4><input id='title0'>" +
            "</input><h4>Description</h4><textarea id='desc' maxlength='500'></textarea>" +
            "<h4>Altitude (meters)</h4><input type='number' id='alt'><h4>Image</h4><input type='text' id='imgUrl' autocomplete='off'></input><div class='imgAutoComplete'></div>" +
            "<input id='pic' type='file' name='pic' accept='image/*'><button class='upload eB' type='button'>Upload</button>" +
            "<div id='progressbar' class='progressbar'></div><button class='cancelUpload eB' type='button' id='cU-0'>Cancel</button><h4>Preview</h4>" +
            "<img id='imgPreview' class='imgPreview' title='Click to enlarge' onload=admin.measureWidth() onerror=`this.onerror=null; this.src='placeholder.png'`>" +
            "<h4>Image height greater than width?</h4><input type='checkbox' name='foo' id='hgw'></input>" +
            "<h4>Coordinates</h4><input class='coor' type='number' step='0.0000001' id='la'>" +
            "<input class='coor' type='number' step='0.0000001' id='lo'><h4 id='scoreDisplay'>Score: <span class='scoreAgg'><span></h4>" +
            "<button id='calcScore' class='eB' type='button'>Calculate score</button><button id='switchToReviews' class='eB' type='button'>Reviews</button>" +
            "<button id='submit0' type='button'>Submit</button><p id='statusMessage0' class='statusM'></p></form><form class='editForm edit2'>" +
            "<h4>Title</h4><input id='title2' type='text'><h4 id='extLine'>Extension: <span id='extType'></span></h4><h4 id='sizeLine'>Size: <span id='size'></span></h4>" +
            "<h4 id='usedByLine'> Used by: <span id='usedList'></span></h4><div id='uploadBox'></div><h4 id='previewLine'>Preview</h4>" +
            "<img id='imgPreview2' class='imgPreview' title='Click to enlarge' onerror=`this.onerror=null; this.src='placeholder.png'`>" +
            "<button class='upload2 eB' type='button'>Replace</button>" +
            "<button id='submit2' type='button'>Submit</button><div id='progressbar2' class='progressbar'></div>" +
            "<button class='cancelUpload eB' type='button' id='cU-2'>Cancel</button>" +
            "<p id='statusMessage2' class='statusM'></p></form></div><div id='confirmation' title='Discard changes?'>" +
            "<p>You have made changes and not submitted them. Do you wish to discard them?</p></div><div id='generalPrompt'><p id='general'></p></div>" +
            "<div id='deleteItem' title='Deletion warning'><p id='del'>Are you sure you want to delete the item <span id='itemToDelete'></span>?</p></div>" +
            "<div id='errorPrompt' title='Error'><p id='errorDesc'></p></div><div id='replaceBox' title='Replace image'>" +
            "<input id='pic3' class='active' type='file' name='pic3' accept='image/*'><button class='upload3 eB' type='button'>Upload</button>" +
            "<div class='progressbar' id='progressbar3'></div><p class='statusM' id='statusMessage3'></p></div>");
        await urlParser(true);
        $(".moreOptions,.dbObjs,.rightBar").on("scroll",function(e){
            checkIfScrollButtonsNeeded($(this));
        });
        $("#slider1").slider({
            min: 0,
            max: 4000,
            step: 10,
            values: [0, 4000],
            orientation: "horizontal",
            range: true,
            slide: function (e, ui) {
                const j = $(this).attr("id").split("r")[1];
                for (let i = 0; i < ui.values.length; ++i) {
                    $(`input.sN${j}[data-index=${i}]`).val(ui.values[i]);
                }
            }
        });
        $("#slider2").slider({
            min: 1,
            max: 150,
            step: 1,
            value: 5,
            orientation: "horizontal",
            range: "min",
            slide: function (e, ui) {
                $("#searchRange").val(ui.value);
            }
        });
        $("#slider3, #slider4").slider({
            min: 1,
            max: 5,
            step: 0.1,
            values: [1, 5],
            orientation: "horizontal",
            range: true,
            slide: function (e, ui) {
                const j = $(this).attr("id").split("r")[1];
                for (let i = 0; i < ui.values.length; ++i) {
                    $(`input.sN${j}[data-index=${i}]`).val(ui.values[i]);
                }
            }
        });
        $("#slider5").slider({
            min: 0,
            max: 4000,
            step: 1,
            values: [1, 4000],
            orientation: "horizontal",
            range: true,
            slide: function (e, ui) {
                for (let i = 0; i < ui.values.length; ++i) {
                    $("input.sR[data-index=" + i + "]").val(ui.values[i]);
                }
            }
        });
        $("#la-search, #la").attr({
            "max": 90,
            "min": -90
        });
        $("#lo-search, #lo").attr({
            "max": 179.9999999,
            "min": -179.9999999
        });
        $(".rT").attr({
            "max": 5,
            "min": 1
        });
        elem = [$("#statusMessage0"), $("#statusMessage2"), $("#statusMessage3")];
        setSize();
    }
    function calcSizeF(input) {
        return (Math.floor(input.size / 1000));
    }
    function setSize() {
        ww = window.innerWidth;
        wh = window.innerHeight;
        if (ww <= 1200) {
            $(".advanced").html("<i class='fas fa-sliders-h'></i>");
        }
        else {
            $(".advanced").html("Advanced...");
        }
        if (ww <= 900) {
            $("#link-0").html("<i class='fas fa-map-marker-alt'></i>");
            $("#link-1").html("<i class='fas fa-star-half-alt'></i>");
            $("#link-2").html("<i class='fas fa-images'></i>");
            sub = 55;
        }
        else {
            $("#link-0").html("Locations");
            $("#link-1").html("Reviews");
            $("#link-2").html("Images");
            $(".rightBar").removeClass("active");
            sub = 90;
        }
        if (ww <= 600) {
            expandReviewsList();
        }
        if ($(".imagePopup").hasClass("enabled")) {
            measureWidth();
        }
    }
    function inject(input) {
        if (site == 0) {
            let scoreVal = input.score;
            if (!scoreVal) scoreVal = "Not rated yet";
            return `${te}<h3>${input.title}</h3>` +
                `<ul><li class='objDesc'>${input.desc}</li><li class='altitude'><b>Altitude:</b> <span class='altitudeSpan'>${input.alt}</span> m</li>` +
                `<li class='hgw'><b>Image width greater than height:</b> <span class='hgwValue'>${input.hgw}</span>` +
                `<li><b>Image url: </b><span class='imgUrl'>${input.img}</span></li><li><b>Coordinates: Lt: </b>` +
                `<span class='La'>${input.pos.coordinates[0]}</span>,  <b>Ln:</b> <span class='Lo'>` +
                `${input.pos.coordinates[1]}</span></li><li><b>Average rating:</b> <span class='score'>${scoreVal}</span></ul></div>`;
        }
        if (site == 1) {
            let extra = "";
            if (input.location.title) extra = `<li><b>Location:</b> <a href='#locations?objectid=${input.location._id}'>` +
                `<span class='locationReview'>${(input.location.title).toString()}</span></a></li>`;
            return `${te}<h3> By user:` +
                `<span class='reviewUser' id='r${Math.random() * 300000}-${input.location._id}'>${input.user}</span></h3>` +
                `<ul><li><b>Score:</b> <span class='score'>${input.score}</span></li>${extra}</ul></div>`;
        }
        else {
            const generatedString = checkIfUsedBy(input.usedBy);

            let calcSize = calcSizeF(input);
            return `${te}<h3>${input.title}</h3><ul><li><b>Extension:</b> <span class='ext'>${input.type}</span></li><li><b>Size: </b>` +
                `<span class='sizeValue' data-index='${input.size}'>${calcSize} kb</span></li>` +
                `<li><b>Used by:</b> <span class='usedByString'>${generatedString}</span></li></ul></div>`;
        }
    }
    function checkIfUsedBy(input) {
        let generatedString = "N/A";
        if (input.length > 0) {
            generatedString = "";
            for (let i = 0; i < input.length; i++) {
                generatedString += `<a href="#locations?objectid=${input[i]._id}" class='ref${i}'>${(input[i].name).toString()}</a>`;
                if (i < input.length - 1) {
                    generatedString += ", ";
                }
            }
        }
        return generatedString;
    }
    async function sendReq(method, query, dest, headers, id, body) {
        controller = new AbortController();
        signal = controller.signal;
        try {
            const res = await fetch(`../api/${dest}${id}${query}`, {
                method: method,
                mode: 'cors',
                headers: headers,
                body: body,
                signal: signal
            });
            if (res.status >= 200 && res.status < 400) {
                return res.json()
            }
            else {
                throw (res);
            }
        }
        catch (err) { throw err };
    }
    $(window).bind("resize", function (e) {
        setSize();
        if(site != 1) checkIfScrollButtonsNeeded($(`#mO${site}`));
        checkIfScrollButtonsNeeded($(".dbObjs"));
        checkIfScrollButtonsNeeded($(".rightBar"));
    });
    $(document).on("mousedown",".fa-arrow-circle-up,.fa-arrow-circle-down",function(e){
        if(e.button == 0){
            const eTM = $(this).parent().parent().parent();
            if($(this).parent().hasClass("up")){
                eTM.animate({ scrollTop: 1 });
            }
            else {
                const scrollH = eTM.prop("scrollHeight") - eTM.height();
                eTM.animate({ scrollTop: scrollH });
            }
        }
    });
    function checkIfScrollButtonsNeeded(obj){
        if(obj.height() < obj.prop("scrollHeight")){
            if(obj.scrollTop() < obj.prop("scrollHeight")-(obj.height()+10)){
                obj.find(".aC.down").addClass("active");
            }
            else {
                obj.find(".aC.down").removeClass("active");
            }
            if(obj.scrollTop() > 10){
                obj.find(".aC.up").addClass("active");
            }
            else {
                obj.find(".aC.up").removeClass("active");
            }
        }
        else {
            obj.find(".aC").removeClass("active");
        }
    }
    $(document).on("keydown", ".control", function (e) {
        let keyCode = e.keyCode || e.which;
        if (keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            if (site == 0) {
                searchLocations();
            }
            else if (site == 1) {
                searchReviews();
            }
            else {
                searchImages();
            }
        }
    });

    $(document).on("mousedown", function (e) {
        if (e.button == 0) {
            $(".userMenu").addClass("disabled");
            $("#link-3").removeClass("active");
            $(".bubble").removeClass("enabled");
        }
    });
    $(document).on("mousedown", ".fa-user", function (e) {
        e.stopPropagation();
        if (e.button == 0) {
            $(".userMenu").toggleClass("disabled");
            $("#link-3").toggleClass("active");
            $(".bubble").removeClass("enabled");
        }
    });
    $(document).on("mousedown", ".deleteUploadedItem", async function (e) {
        if (e.button == 0) {
            $("#pic3").replaceWith($("#pic3").val('').clone(true));
            $("#pic3").addClass("active");
            $("#replaceFile").remove();
            $(".ui-dialog").find("button:contains('Save')").attr('disabled', true);
            $("#replaceBox .eB").text("Upload").removeClass("cancelUpload deleteUploadedItem active").addClass("upload3");
            try {
                await sendReq("delete", "?temp=true", "images/", { "x-auth-token": cook }, `${replacedFile.title}%2E${replacedFile.type}`);
            }
            catch (ex) {
                $("#statusMessage3").html(ex);
            }
        }
    });
    $(document).on("mousedown", ".upload2", async function (e) {
        if (e.button == 0) {
            const keepUploadedImage = await uploadWindow();
            if (keepUploadedImage) {
                $("#extType").html(replacedFile.type);
                const rSize = Math.floor((parseInt(replacedFile.size)) / 1000);
                $("#size").html(`${rSize} kb`).attr("class", replacedFile.size);
                updateImg(`/temp/${replacedFile.title}.${replacedFile.type}`, 2, true);
            }
        }
    });
    $(document).on("mousedown", ".upload3", async function (e) {
        if (e.button == 0) {
            let file = document.getElementById("pic3").files[0];
            let formdata = new FormData()
            formdata.append('file', file);
            $("#statusMessage3").html("").removeClass("green");
            $("#replaceBox .eB").text("Cancel").removeClass("upload3").addClass("cancelUpload");
            $("#progressbar3").addClass("active").progressbar({
                value: false
            });
            try {
                const result = await sendReq("post", "?temp=true", "images", { "x-auth-token": cook }, "", formdata);
                if (result) {
                    $("#progressbar3").removeClass("active");
                    $("#replaceBox .eB").text("Delete").removeClass("cancelUpload").addClass("deleteUploadedItem");
                    $("#pic3").removeClass("active");
                    successMsg([{ title: `"${result.fileStats.title}.${result.fileStats.type}"` }, "uploaded"], elem[2]);
                    let fileName = result.fileStats.title;
                    if (fileName.length > 25) {
                        fileName = replaceRange(fileName, 12, (14 + (fileName.length - 25)), "...");
                    }
                    fileName += `.${result.fileStats.type}`;
                    const rSize = Math.floor((parseInt(result.fileStats.size)) / 1000);
                    const str = $(`<div id='replaceFile'><span id='replacedFileName'>${fileName}</span><span id='replaceSize'>${rSize} kb</span</div>`);
                    str.insertBefore(".deleteUploadedItem");
                    $(".ui-dialog").find("button:contains('Save')").removeAttr("disabled");
                    replacedFile = result.fileStats;
                }
            }
            catch (ex) {
                $("#progressbar3").removeClass("active");
                $("#replaceBox .eB").text("Upload").removeClass("cancelUpload").addClass("upload3");
                const c = await convertError(ex);
                $("#statusMessage3").html(c);
            }
        }
    });
    function replaceRange(s, start, end, substitute) {
        return s.substring(0, start) + substitute + s.substring(end);
    }
    $(document).on("mousedown", ".userMenu, .bubble", function (e) {
        e.stopPropagation();
    });
    $(document).on("submit", ".pwForm", async function (e) {
        e.preventDefault();
        try {
            const _id = $(".fa-user").attr("id");
            const body = {
                password: $("#oldPw").val(),
                newPassword: $("#newPw").val(),
                repeatPassword: $("#repeatNewPw").val()
            }
            const res = await sendReq('put', "", "users/", {
                "x-auth-token": cook, 'Accept': 'application/json',
                'Content-Type': 'application/json'
            }, _id, JSON.stringify(body));
            if (res) {
                if (res.token) {
                    let d = new Date();
                    d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
                    const expires = "expires=" + d.toUTCString();
                    document.cookie = "token =" + res.token + ";" + expires + ";path=/";
                    retrieveCookie();
                }
                $("#statusMessagePassword").addClass("green").html(res.msg);
                setTimeout(function () {
                    $("#oldPw, #newPw, #repeatNewPw").val("");
                    $(".bubble").removeClass("enabled");
                }, 4000);
            }
        }
        catch (ex) {
            const c = await convertError(ex);
            $("#statusMessagePassword").html(c);
        }
    });
    $(document).on("keyup mouseup", "#imgUrl", function (e) {
        chosen = false;
        imgAutoComplete($(this).val());
    });
    $(document).on("focusout", "#imgUrl", async function (e) {

        autoFix();

        setTimeout(function () {
            updateImg($("#imgUrl").val(), "", true);
        }), 50;
    });
    async function uploadWindow() {
        return new Promise((resolve) => {
            let r = true;
            $("#pic3").replaceWith($("#pic3").val('').clone(true));
            $("#replaceBox").dialog({
                resizable: false,
                height: "auto",
                width: 340,
                modal: true,
                buttons: {
                    Save: function () {
                        r = false;
                        $(this).dialog("close");
                        $(".deleteUploadedItem").removeClass("active");
                        return resolve(true);
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                open: function () {
                    $(this).dialog('widget').find("button:contains('Save')").attr('disabled', true);
                    $("#replaceBox .eB").text("Upload").removeClass("cancelUpload deleteUploadedItem").addClass("upload3");
                    $("#statusMessage3").html("");
                    $("#pic3").addClass("active");
                    $("#replaceFile").remove();
                },
                close: function () {
                    if (r) {
                        controller.abort();
                        $(".upload3").removeClass("active");
                        return resolve(false);
                    }
                }
            });
        });

    }
    async function imgAutoComplete(query) {
        let ext;
        let title;
        let finalQuery = `?title=${query}&limit=10`;
        $(".imgAutoComplete").html("");
        if (query.indexOf('.') > -1) {
            title = query.split('.')[0];
            ext = query.split('.')[1];
            finalQuery = `?title=${title}&ext=${ext}&limit=10`;
        }
        try {
            const results = await sendReq('get', finalQuery, 'images', { 'x-auth-token': cook }, "", null);
            firstResultImage = results[0];
            for (let i = 0; i < results.length; i++) {
                $(".imgAutoComplete").append(`<p class='imgAutoList ${results[i]._id}'>${results[i].title}.${results[i].type}</p>`);
            }
        }
        catch (ex) {
        }
    }
    $(document).on("mouseenter", ".imgAutoComplete", function (e) {
        hovered2[0] = true;
        openCloseAuto();
    }).on("mouseleave", ".imgAutoComplete", function (e) {
        hovered2[0] = false;
        openCloseAuto();
    });
    $(document).on("mousedown", ".imgAutoList", function (e) {
        if (e.button == 0) {
            chosen = true;
            $("#imgUrl").val($(this).text());
            $(".imgAutoComplete").html("").removeClass("active");
        }
    });
    $(document).on("mouseenter", ".imgAutoList", function (e) {
        hovered2[1] = true;
        openCloseAuto();
    }).on("mouseleave", ".imgAutoList", function (e) {
        hovered2[1] = false;
        openCloseAuto();
    });
    $(document).on("mousedown", ".imgPreview", function (e) {
        if (e.button == 0) {
            $(".imagePopup").addClass("enabled");
            $(".popupPreview").attr("src", $(this).attr("src"));
        }
    });
    $(document).on("change", "")
    $(document).on("keyup mouseup", ".dI", function (e) {
        const ind = $(this).attr("class").split("sN")[1].split(" ")[0];
        const toChange = $(this).attr("data-index");
        const inputField = $(this);
        $(`#slider${ind}`).slider('values', toChange, inputField.val());
    });
    $(document).on("keyup mouseup", "#searchRange", function (e) {
        let $this = $(this);
        $("#slider2").slider("value", $this.val());
    });
    $(document).on("mousedown", ".changePw", function (e) {
        if (e.button == 0) {
            $(".userMenu").addClass("disabled");
            $(".bubble").addClass("enabled");
            $("#statusMessagePassword").html("").removeClass("green");
        }
    });
    $(document).on("mousedown", ".closeForm", function (e) {
        if (e.button == 0) {
            $(".bubble").removeClass("enabled");
        }
    });
    $(document).on("mousedown", ".fa-edit", function (e) {
        if (e.button == 0 && ww < 900) {
            $(`.edit${site},.rightBar`).addClass("active");
            $(".noItems").addClass("disabled");
            const id = $(this).parent().parent().attr("id");
            selected[(site / 2)] = (objectList.find(x => x._id === id));
            first[(site / 2)] = false;
        }
    });
    function measureWidth() {
        const obj = $(".popupPreview");
        if (obj.width() >= (ww * 0.995)) {
            obj.addClass("wide");
        }
        else if (obj.height() >= wh - sub) {
            obj.removeClass("wide");
        }
    }
    function updateUrl(query) {
        let stateObj = {
            foo: "bar"
        };
        window.history.pushState(stateObj, 'Title', query);
    }
    function updateEdit(iObj, id) {
        updateUrl(`#${addP[site]}?objectid=${id}`);
        $(".oL, .overLay").removeClass("active");
        iObj.addClass("active");
        $(`.${id}`).addClass("active");
        selected[(site / 2)] = (objectList.find(x => x._id === id));
        $(`#submit${site}`).attr("class", $(iObj).attr("id")).text("Submit");
        $(`#statusMessage${site}`).html("");
        $(`#title${site}`).val($(iObj).children("h3").text()).attr("placeholder", "");
        if (site == 0) {
            if (selected[0].score != "Not rated yet") selected[0].score = parseFloat(selected[0].score);
            $("#desc").val($(iObj).find(".objDesc").text());
            $("#scoreDisplay").removeClass("disabled");
            $("#alt").val(parseInt($(iObj).find(".altitudeSpan").text()));
            if ($(iObj).find(".hgwValue").text() == "true") {
                $("#hgw").prop('checked', true).attr('checked', 'checked');
            }
            else {
                $("#hgw").prop('checked', false);
            }
            $("#imgUrl").val($(iObj).find(".imgUrl").text());
            updateImg($("#imgUrl").val(), "", setNew);
            $("#la").val(parseFloat($(iObj).find(".La").text()));
            $("#lo").val(parseFloat($(iObj).find(".Lo").text()));
            $(".scoreAgg").html($(iObj).find(".score").text());
            setNew = false;
        }
        else {
            $("#extLine, #sizeLine, #usedByLine, #previewLine, #imgPreview2, .upload2").removeClass("disabled");
            $("#extType").html($(iObj).find(".ext").text());
            $("#size").html($(iObj).find(".sizeValue").text()).attr("class", $(iObj).find(".sizeValue").attr("data-index"));
            $("#uploadBox").removeClass("active");
            $("#usedList").html($(iObj).find(".usedByString").html());
            updateImg(`${$(iObj).children("h3").text()}.${$(iObj).find(".ext").text()}`, 2, true);
            $("#uploadBox").html("");
            if (id) {
                delete selected[1].usedBy;
                delete selected[1].score;
            }
        }
        checkIfScrollButtonsNeeded($(".rightBar"));
    }
    $(window).on('hashchange', function () {
        urlParser(true);
    });
    async function urlParser(selObj) {
        let para = null;
        if (document.URL.indexOf("?objectid=") > -1) {
            para = document.URL.split('?objectid=')[1];
        }
        if (document.URL.indexOf("#reviews") > -1) {
            if (document.URL.indexOf("?location=") > -1 || document.URL.indexOf("?user=") > -1) {
                para = document.URL.split('?')[1];
            }
            switchPage(1, para, selObj);
        }
        else if (document.URL.indexOf("#images") > -1) {
            switchPage(2, para, selObj);
        }
        else {
            switchPage(0, para, selObj);
        }
    }
    function compareTwo(obj, s) {
        if (s == 0 && selected[0]) {
            delete obj.score;
            delete selected[0].score;
        }
        if (JSON.stringify(obj) == JSON.stringify(selected[(s / 2)])) {
            return false;
        }
        return true;
    }
    $(document).on("mousedown", ".logout", function (e) {
        if (e.button == 0) {
            document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "../login";
        }
    });
    $(document).on("mousedown", "#switchToReviews", function (e) {
        $("#link-0").removeClass("active");
        const id = $("#submit0").attr("class");
        $("#searchField").addClass(id);
        $(".confirmedSearch-2").removeClass("active");

        confirmedSearch(id, $("#title0").val(), 1);
        updateUrl('#reviews?location=' + id);
        urlParser(true);
    });
    $(document).on("mousedown", ".link", function (e) {
        if (e.button == 0) {
            cleanUrl();
            if ($(this).attr("href").split('#')[1] == addP[site]) {
                urlParser(false);
            }
        }
    });
    $(document).on("change", "#pic3", function () {
        $(".upload3").addClass("active");
    });
    function cleanUrl() {
        let uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            let clean_uri = uri.substring(0, uri.indexOf("?"));
            window.history.replaceState({}, document.title, clean_uri);
        }
    }
    async function switchPage(p, _id, selObj) {
        site = p;
        $("#searchField").val("");
        $(".insReviews").html("Search reviews by searching for locations (or through the 'locations' tab) or users above. User queries must start with &quot;User:&quot;");
        $(".autoComplete, .moreOptions, .advanced, .link, .rightBar,#link-" + p).removeClass("active");
        $("#link-" + p).addClass("active");
        $(".dbObjs, .insReviews").removeClass("extended").removeClass("extended2").removeClass("extendedReviews");
        $(".oL, .overLay").remove();
        if (p == 0 || p == 2) {
            if (_id == null) {
                _id = "";
            }
            $("#searchField").attr("placeholder", "Search by title...").removeClass("searchReviews").removeClass("searchImages");
            $(".insReviews").addClass("left");
            $(".confirmedSearch-1, .insReviews, .confirmedSearch-2, .filterHolder, .editForm, .cS").removeClass("active");
            $(".leftBar, .insReviews").removeClass("extended");
            $(".dbObjs .oF").removeClass("searchReviews");
            $(".rightBar, .plus, .advanced, .buttonContainer").removeClass("disabled");
            await insertIntoList("", "", addP[p]);
            let found = (objectList.find(x => x._id === _id));
            const obj = collectObj(site);
            if (!first[(p / 2)] && !found && selObj) {
                $(".noItems").addClass("disabled");
                $(`.edit${site}`).addClass("active");
                if ($(`#submit${site}`).attr("class") != "newItem") {
                    const sel = $(`#submit${site}`).attr("class");
                    $(`#${sel}, .overLay.${sel}`).addClass("active");
                    const isChanged = compareTwo(obj, site);
                    selected[(p / 2)] = (objectList.find(x => x._id === sel));
                    if (ww < 900 && isChanged) $(".rightBar").addClass("active");
                }
            }
            else if (found && selObj) {
                first[(p / 2)] = false;
                $(".noItems").addClass("disabled");
                $(`.edit${site}`).addClass("active");
                $(`#${_id}, .overLay.${_id}`).addClass("active");
                selected[(p / 2)] = found;
                updateEdit($(`#${_id}`), _id);
            }
            else {
                first[(p / 2)] = true;
                $(`.edit${site}`).removeClass("active");
                $(".noItems").removeClass("disabled");
            }
            if (p == 0) {
                $(".moreOptions").removeClass("short");
                $("#searchField").removeAttr("class");
                $(".plus").attr("title", "Add new location");
            }
            else {
                $("#searchField").addClass("searchImages");
                $(".moreOptions").addClass("short");
                $(".plus").attr("title", "Add new image");
                if (selected[1]) {
                    delete selected[1].usedBy;
                    delete selected[1].score;
                }
            }
            checkIfScrollButtonsNeeded($(".rightBar"));
        }
        else if (p == 1) {
            $("#searchField").attr("placeholder", "Enter search query...");
            $(".insReviews").removeClass("left");
            $(".insReviews, .filterHolder").addClass("active");
            $("#searchField, .dbObjs .oF").addClass("searchReviews");
            $(".searchButton").addClass("searchReviewsButton");
            if ($(".confirmedSearch-1").attr("id") != undefined) {
                $(".confirmedSearch-1, .cS").addClass("active");
                $(".dbObjs").addClass("extendedReviews");
            }
            if ($(".confirmedSearch-2").attr("id") != undefined) {
                $(".confirmedSearch-2, .cS").addClass("active");
                $(".dbObjs").addClass("extendedReviews");
            }
            expandReviewsList();
            $(".leftBar").addClass("extended");
            $(".rightBar, .plus, .advanced, .buttonContainer").addClass("disabled");
            if (_id != null) {
                let para2 = "";
                let dest;
                if ((_id).indexOf("&") > -1) {
                    para2 = (_id.split('&')[1]);
                    _id = _id.split('&')[0];
                }
                if ((para2).indexOf("location=") > -1 || para2 == "" && (_id).indexOf("user=") > -1) {
                    dest = "user";
                    _id = _id.split('=')[1];
                    para2 = para2.split('=')[1];
                    confirmedSearch(_id, _id, 2);
                }
                else if ((para2).indexOf("user=") > -1 || para2 == "" && (_id).indexOf("location=") > -1) {
                    dest = "location";
                    _id = _id.split('=')[1];
                    if (para2 != "") {
                        para2 = para2.split('=')[1];
                        confirmedSearch(para2, para2, 2);
                    }
                }
                await insertIntoList(_id, `?${para2}`, dest);
                if (dest == "location" && objectList.length > 0) {
                    confirmedSearch(_id, objectList[0].location.title, 1);
                }
            }
        }
        checkIfScrollButtonsNeeded($(".dbObjs"));
    }
    async function autoFix() {
        if (!chosen) {
            if (!firstResultImage) {
                try {
                    firstResultImage = await sendReq('get', '?limit=1', 'images', { 'x-auth-token': cook }, "", null);
                }
                catch (err) {
                    errorPrompt(err);
                    return false;
                }
            }
            $("#imgUrl").val(`${firstResultImage.title}.${firstResultImage.type}`);
        }
    }
    function collectObj(s) {
        if (s == 0) {
            const uO = {
                pos: {
                    coordinates: [parseFloat($("#la").val().trim()), parseFloat($("#lo").val().trim())],
                    type: "Point"
                },
                _id: $("#submit0").attr("class"),
                title: $("#title0").val().trim(),
                desc: $("#desc").val().trim(),
                alt: parseInt($("#alt").val().trim()),
                hgw: $("#hgw").prop('checked'),
                img: $("#imgUrl").val().trim(),
                score: $(".scoreAgg").text()
            }
            if (uO.score != "Not rated yet") {
                uO.score = parseFloat(uO.score);
            }
            return uO;
        }
        else {
            let uO = {
                _id: $("#submit2").attr("class"),
                title: $("#title2").val().trim(),
                type: $("#extType").text(),
                size: parseInt($("#size").attr("class"))
            }
            if ($("#pic2").length > 0) {
                if (document.getElementById("pic2").files.length > 0) {
                    uO.file = true;
                }
            }
            if ($("#submit2").hasClass("newItem")) {
                delete uO._id;
                delete uO.type;
                delete uO.size;
            }
            return uO;
        }
    }
    function checkForm(s) {
        let invalid = true;
        if (s == 0) {
            $(".edit0 :text, .edit0 :file, .edit0 :checkbox, .edit0 select, .edit0 textarea").each(function () {
                if ($(this).val() != "" && $(this).val() != "placeholder.png" && $(this).val() != "on") {
                    invalid = false;
                }
            });
        }
        else {
            if ($("#title2").val() != "" || $("#pic2").val() != "" && $("#pic2").length > 0) {
                invalid = false;
            }
        }
        return invalid;
    }
    $(document).on("keyup mouseup", ".searchReviews", function (e) {
        if ($(this).val().length > 0) {
            const patt = /User:/i;
            let sQ = $(this).val().trim();
            if (patt.test(sQ)) {
                let toSend = sQ.split(':')[1];
                if (toSend.length > 0) {
                    toSend = `id=${toSend}`;
                }
                sendReq('get', `?${toSend}`, 'ips', { 'x-auth-token': cook }, "", null).then(data => {
                    $(".autoComplete").addClass("active");
                    $(".autoComplete").html("");
                    for (let i = 0; i < data.length; i++) {
                        $(".autoComplete").append(`<p class='searchByUser'>${data[i]._id}</p>`)
                    }
                }).catch(err => {
                });
            }
            else {
                sendReq(`get`, `?title=${$(this).val()}&limit=10&titleonly=true`, 'locations', { 'x-no': true }, "", null).then(data => {
                    let returnedData = data.data;
                    $(".autoComplete").addClass("active");
                    $(".autoComplete").html("");
                    for (let i = 0; i < returnedData.length; i++) {
                        $(".autoComplete").append(`<p class='${returnedData[i]._id}'>${returnedData[i].title}</p>`)
                    }
                }).catch(err => {
                });
            }
        }
    });
    $(document).on("mouseenter", ".autoComplete", function (e) {
        hovered[0] = true;
    }).on("mouseleave", ".autoComplete", function (e) {
        hovered[0] = false;
        searchField();
    });
    $(document).on("mouseenter", ".autoComplete p", function (e) {
        hovered[1] = true;
    }).on("mouseleave", ".autoComplete p", function (e) {
        hovered[1] = false;
        searchField();
    });
    $(document).on("mousedown", ".autoComplete p", function (e) {
        if (e.button == 0) {
            $(".autoComplete").removeClass("active");
            const classVal = $(this).attr("class");
            const title = $(this).text();
            if ($(".autoComplete p").hasClass("searchByUser")) {
                confirmedSearch($(this).text(), title, 2);
            }
            else {
                confirmedSearch(classVal, title, 1);
            }
        }
    });
    function confirmedSearch(c, t, v) {
        $(".confirmedSearch-" + v + ",.cS").addClass("active").attr("id", ("s-" + c));
        $(`.confirmedSearch-${v} span`).text(t);
        $(".dbObjs").addClass("extendedReviews");
        expandReviewsList();
    }
    $(document).on("mousedown", ".cross2", function (e) {
        if (e.button == 0) {
            if ($(this).attr("id") == "cross") {
                $(".confirmedSearch-1").removeClass("active").removeAttr("id");
            }
            else {
                $(".confirmedSearch-2").removeClass("active").removeAttr("id");
            }
            expandReviewsList();
            if ((!$(".confirmedSearch-1").hasClass("active")) && (!$(".confirmedSearch-2").hasClass("active"))) {
                $(".dbObjs").removeClass("extendedReviews");
            }
        }
    });
    $(document).on("mousedown", ".rightBar .cross", function (e) {
        if (e.button == 0) {
            if (site == 0) {
                switchObj($(`#${$("#submit").attr("class")}`), true);
            }
            else {
                switchObj($(`#${$("#submit2").attr("class")}`), true);
            }
        }
    });
    function searchField() {
        if (!hovered[0] && !hovered[1]) {
            $(".autoComplete").html("").removeClass("active");
        }
    }
    function openCloseAuto() {
        if (!hovered2[0] && !hovered2[1]) {
            $(".imgAutoComplete").html("").removeClass("active");
        }
    }
    $(document).on("mousedown", ".oL", function (e) {
        if (e.button == 0 && site != 1) {
            if (ww >= 900) {
                switchObj($(this), false);
            }
            else {
                updateEdit($(this), $(this).attr("id"));
            }
            checkIfScrollButtonsNeeded($(".rightBar"));
        }
    });
    function execDec(listed, close, clId) {
        if (close) {
            $(".rightBar").removeClass("active");
        }
        else {
            updateEdit(listed, clId);
        }
    }
    async function switchObj(listed, close) {
        if (site == 0 || site == 2) {
            let method;
            let prompt = false;
            let clId = listed.attr("id");
            const updatedObject = collectObj(site);
            if (first[(site / 2)] == true) {
                $(".noItems").addClass("disabled");
                if (ww >= 900) execDec(listed, close, clId);
                first[(site / 2)] = false;
                $(`.edit${site}`).addClass("active");
            }
            else {
                if ($(`#submit${site}`).attr("class") == "newItem") {
                    method = 'post';
                    const isFormEmpty = checkForm(site);
                    if (!isFormEmpty) {
                        prompt = true;
                    }
                    else {
                        execDec(listed, close, clId);
                    }
                }
                else {
                    const isChanged = compareTwo(updatedObject, site);
                    if (isChanged == false) {
                        execDec(listed, close, clId);
                    }
                    else {
                        prompt = true;
                        method = 'put';
                    }
                }
                if (prompt) {
                    const send = await confirmClose();
                    if (send) {
                        if (send[0] && send[1]) {
                            try {
                                const data = await sendToServer(updatedObject, method, (site / 2));
                                successMsg(data, elem[site / 2]);
                                execDec(listed, close, clId);
                            }
                            catch (ex) {
                                errorPrompt(ex);
                            }
                        }
                        else if (send[0] && !send[1]) {
                            execDec(listed, close, clId);
                        }
                    }
                }
            }
        }
    }
    function successMsg(data, obj) {
        obj.html(`Item ${data[0].title} ${data[1]} successfully!`).addClass("green");
        $(".rightBar").scrollTop($(".rightBar").prop("scrollHeight"));
        setTimeout(function () {
            obj.removeClass("green").html("");
        }, 4000);
    }
    function confirmClose() {
        return new Promise((resolve) => {
            let r = true;
            $("#confirmation").dialog({
                resizable: false,
                height: "auto",
                width: 340,
                modal: true,
                buttons: {
                    "Submit": function () {
                        r = false;
                        $(this).dialog("close");
                        return resolve([true, true]);
                    },
                    "Discard": function () {
                        r = false;
                        $(this).dialog("close");
                        return resolve([true, false]);
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    },
                },
                close: function () {
                    if (r) return resolve(false);
                }
            });
        });

    }
    $(window).on("beforeunload", function () {
        const sB = [$("#submit0"), $("#submit2")];
        for (let i = 0; i < 2; i++) {
            const obj = collectObj(i * 2);
            const isChanged = compareTwo(obj, (i * 2));
            const isFormEmpty = checkForm(i * 2);
            if (isChanged && !sB[i].hasClass("newItem") && $(".rightBar").hasClass("active") && ww < 900 && !first[i] ||
                sB[i].hasClass("newItem") && !isFormEmpty && $(".rightBar").hasClass("active") && ww < 900 && !first[i] ||
                isChanged && !sB[i].hasClass("newItem") && ww >= 900 && !first[i] ||
                sB[i].hasClass("newItem") && !isFormEmpty && ww >= 900 && !first[i]) {
                i = 2;
                return "You have unchanged changes. Are you sure you want to leave without saving?";
            }
        }
        return;
    });
    function convertError(err) {
        return new Promise(resolve => {
            let extracted = "";
            if (err.status) {
                err.json().then(data => { extracted = data.msg; if (!extracted) extracted = data; return resolve(extracted) }).catch(ex => {
                    extracted = err.statusText;
                    return resolve(extracted);
                });
            }
            else {
                return resolve("No connection, or the operation was aborted");
            }
        });
    }
    async function deleteItem(saveObject) {
        let result,
            reviewUser,
            deleteId = saveObject.parent().parent().attr("id");
        if (site == 1) {
            deleteId = (saveObject.parent().parent().find(".reviewUser").attr("id")).split('-')[1];
            reviewUser = saveObject.parent().parent().find(".reviewUser").text();
        }
        try {
            const data = await sendReq('delete', "", `${addP[site]}/`, {
                'x-auth-token': cook,
                'x-user': reviewUser
            }, `${deleteId}`, null);
            result = data;
            const i = objectList.findIndex(x => x._id === result._id);
            objectList.splice(i, 1);
            $("#generalPrompt").attr('title', 'Deletion successful');
            if ($(".oL.active").attr("id") == (`${result._id}`)) cleanUrl();
            $(`#${data._id}, .overLay.${data._id}`).remove();
            first[(site / 2)] = true;
            $(`.edit${site}`).removeClass("active");
            if (site == 0) {
                if ($("#usedList").text().indexOf(data.title) > -1) {
                    const str = $("#usedList").text();
                    const arrayOfObjs = updateArrayOfStrings(str, true, result.title, result.title);
                    const generatedString = checkIfUsedBy(arrayOfObjs);
                    $("#usedList").html(generatedString);
                }
            }
            else if (site == 2) {
                if ($("#imgUrl").val() == `${data.title}.${data.type}`) {
                    $("#imgUrl").val("placeholder.png");
                    updateImg("placeholder.png", "", true);
                }
            }
            $(".noItems").removeClass("disabled");
            $(`#statusMessage${site}`).html("");
            return result;
        }
        catch (err) {
            result = err;
            $("#generalPrompt").attr('title', 'Deletion failed');
            let c = await convertError(err);

            throw c;
        }
    }
    $(document).on("mousedown", ".trash", function (e) {
        e.stopPropagation();
        if (e.button == 0) {
            let saveObject = $(this);
            const itemName = saveObject.parent().parent().find("h3").text();
            $("#itemToDelete").text(itemName);
            $("#deleteItem").dialog({
                resizable: false,
                height: "auto",
                width: 340,
                modal: true,
                buttons: {
                    "Delete": async function () {
                        $(this).dialog("close");
                        try {
                            const result = await deleteItem(saveObject);
                            if (site != 1) {
                                notice = `${result.title}`;
                            }
                            else {
                                notice = `Review from user id: ${result.user} for location: ${result.location.title}`;
                            }
                            $("#general").text(`Result: Item ${(notice)} successfully deleted `);
                            displayRes();
                        }
                        catch (err) {
                            $("#general").text(`Deletion failed. Reason: ${err}`);
                            displayRes();
                        }
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    function displayRes() {
        $("#generalPrompt").dialog({
            resizable: false,
            height: "auto",
            width: 340,
            modal: true,
            buttons: {
                "Okay": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    $(document).on("mousedown", ".upload", async function (e) {
        e.preventDefault();
        if (e.button == 0) {
            let file = document.getElementById("pic").files[0];
            if (file) {
                let formdata = new FormData()
                formdata.append('file', file);
                $("#progressbar, #cU-0").addClass("active");
                $("#progressbar").progressbar({
                    value: false
                });
                try {
                    const data = await sendReq('post', "", "images", { 'x-auth-token': cook }, "", formdata);
                    if (data) {
                        $("#imgUrl").val(`${data.title}.${data.type}`);
                        updateImg(`${data.title}.${data.type}`, "", true);
                        $("#pic").replaceWith($("#pic").val('').clone(true));
                        $("#progressbar, #cU-0").removeClass("active");
                    }
                }
                catch (err) {
                    $("#progressbar, #cU-0").removeClass("active");
                    const c = await convertError(err);
                    errorPrompt(c);
                }
            }
            else {
                errorPrompt("No files have been attached!");
            }
        }
    });
    $(window).bind("resize", function (e) {
        measure();
    });
    $(document).on("mousedown", ".cancelUpload", function (e) {
        if (e.button == 0) {
            controller.abort();
        }
    });
    $(document).on("mousedown", ".popupPreview", function (e) {
        e.stopPropagation();
    });
    $(document).on("mousedown", ".imagePopup", function (e) {
        if (e.button == 0) {
            iClose();
        }
    });
    $(document).on("mousedown", ".imagePopup .cross", function (e) {
        e.stopPropagation();
        if (e.button == 0) {
            iClose();
        }
    });
    function updateImg(url, i, update) {
        let salt = "";
        if (update) salt = `?${Math.random()}`;
        $(`#imgPreview${i}`).attr("src", `../img/${url}${salt}`);
    }
    function iClose() {
        $(".imagePopup").removeClass("enabled");
        $(".popupPreview").removeClass("high");
    }
    $(document).on("mousedown", "#calcScore", async function (e) {
        if (e.button == 0) {
            try {
                const _id = $("#submit0").attr("class");
                const data = await sendReq('get', "", "reviews/scores/", { "x-auth-token": cook }, _id, null);

                const i = objectList.findIndex(x => x._id === _id);
                objectList[i].score = data.score;
                $(".scoreAgg").html(data.score);
                $(`#${_id}`).find(".score").html(data.score);
            }
            catch (err) {
                const c = await convertError(err);
                errorPrompt(c);
            }
        }
    });
    $(document).on("mousedown", ".advanced", function (e) {
        if (e.button == 0 && site != 1) {
            $(this).toggleClass("active");
            const cl = ["extended", "extended2"];
            $(`#mO${site}`).toggleClass("active");
            $(".dbObjs,.insReviews").toggleClass(cl[(site / 2)]);
            checkIfScrollButtonsNeeded($(`#mO${site}`));
        }
    });
    $(document).on("mousedown", ".searchButton", function (e) {
        if (e.button == 0) {
            if (site == 0) {
                searchLocations();
            }
            else if (site == 1) {
                searchReviews();
            }
            else {
                searchImages();
            }
        }
    });
    function updateArrayOfStrings(str, rm, oldName, manipulatedItemName) {
        str = str.split(",");
        let itemIndex;
        for (let i = 0; i < str.length; i++) {
            if (str[i].indexOf(oldName) > -1) {
                itemIndex = i;
                i = str.length;
            }
        }
        if (rm) {
            str.splice(itemIndex, 1);
            let arrayOfObjs = [];
            for (let i = 0; i < str.length; i++) {
                arrayOfObjs[i] = {
                    _id: $(`#usedList .ref${i}`).attr("href").split("=")[1],
                    name: str[i]
                }
            }
            return arrayOfObjs;
        }
        else {
            $(`#usedList .ref${itemIndex}`).text(manipulatedItemName);
        }
        return str;
    }
    function searchReviews() {
        const minScore = $("input.ratingsFilter[data-index=0]").val().trim();
        const maxScore = $("input.ratingsFilter[data-index=1]").val().trim();
        let query;
        let dest;
        let addQ = "";
        let id2;
        let n = 1;
        if ($(".confirmedSearch-1").attr("id") != undefined || $(".confirmedSearch-2").attr("id") != undefined) {
            if ($(".confirmedSearch-1").attr("id") != undefined && $(".confirmedSearch-2").attr("id") == undefined) {
                query = `?minScore=${minScore}&maxScore=${maxScore}`;
                dest = "location";
            }
            else if ($(".confirmedSearch-1").attr("id") == undefined && $(".confirmedSearch-2").attr("id") != undefined) {
                query = `?minScore=${minScore}&maxScore=${maxScore}`;
                dest = "user";
                n = 2;
            }
            else {
                id2 = $(".confirmedSearch-2").attr("id").split('-')[1];
                query = `?user=${id2}&minScore=${minScore}&maxScore=${maxScore}`;
                dest = "location";
                addQ = `&user=${id2}`;
            }
            let id = ($(".confirmedSearch-" + n).attr("id")).split('-')[1];
            updateUrl(`#reviews?${dest}=${id}${addQ}`);
            insertIntoList(id, query, dest);
        }
    }
    function searchImages() {
        let params = {
            title: $(".searchImages").val().trim(),
            ext: $("#extSearch").val().trim(),
            minSize: $("#minSize").val().trim(),
            maxSize: $("#maxSize").val().trim(),
            usedBy: ($("#usedByFilter").val().trim())
        };
        let esc = encodeURIComponent;
        Object.keys(params).forEach((k) => (params[k] == null || params[k] == "") && delete params[k]);
        let query = Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
        insertIntoList("", ("?" + query), 'images');
    }
    function searchLocations() {
        let params = {
            title: $("#searchField").val().trim(),
            desc: $("#descFilter").val().trim(),
            minHeight: $(".sN1[data-index=0]").val().trim(),
            maxHeight: $(".sN1[data-index=1]").val().trim(),
            imgName: $("#imgName").val().trim(),
            minScore: $(".sN3[data-index=0]").val().trim(),
            maxScore: $(".sN3[data-index=1]").val().trim(),
        };

        let esc = encodeURIComponent;
        Object.keys(params).forEach((k) => (params[k] == null || params[k] == "") && delete params[k]);
        if ($("#use").prop("checked")) {
            params.hgw = $("#hwRatio").is(":checked");
        }
        if ($("#la-search").val() && $("#lo-search").val()) {
            params.la = $("#la-search").val();
            params.lo = $("#lo-search").val();
            params.range = $("#searchRange").val()
        }
        let query = Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
        insertIntoList("", ("?" + query), 'locations');
    }
    $(document).on("mousedown", "#submit0, #submit2", async function (e) {
        e.preventDefault();
        if (e.button == 0) {
            $(`#statusMessage${site}`).html("");
            if (site == 0) {
                autoFix();
            }
            const obj = collectObj(site);
            if ($(this).hasClass("newItem")) {
                const isFormEmpty = checkForm(site);
                if (!isFormEmpty) {
                    try {
                        const data = await sendToServer(obj, 'post', (site / 2));
                        updateEdit($(`#${data[0]._id}`), data[0]._id);
                        successMsg(data, elem[site / 2]);
                    }
                    catch (err) {
                        elem[(site / 2)].html('Error: ' + (err));
                        $(".rightBar").scrollTop($(".rightBar").prop("scrollHeight"));
                    }
                }
            }
            else {
                const isChanged = compareTwo(obj, site);
                if (isChanged) {
                    try {
                        const data = await sendToServer(obj, 'put', (site / 2));
                        updateEdit($(`#${data[0]._id}`), data[0]._id);
                        successMsg(data, elem[(site / 2)]);
                        setTimeout(function () {
                            $(".rightBar").removeClass("active");
                        }, 4000);
                    }
                    catch (err) {
                        elem[(site / 2)].html('Error: ' + (err));
                        $(".rightBar").scrollTop($(".rightBar").prop("scrollHeight"));
                    }
                }
                else {
                    $("#general").text("You have not made any changes!");
                    $("#generalPrompt").attr("title", "No changes!");
                    $("#generalPrompt").dialog({
                        resizable: false,
                        height: "auto",
                        width: 340,
                        modal: true,
                        buttons: {
                            "Okay": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        }

    });
    $(document).on("change", "#use", function () {
        $("#toggleLabel").toggleClass("disabled");
        $('#hwRatio').prop('disabled', function (i, v) { return !v; });
    });
    $(document).on("mousedown", ".plus", async function (e) {
        if (e.button == 0) {
            const obj = collectObj(site);
            const isFormEmpty = checkForm(site);
            const isChanged = compareTwo(obj, site);
            if (!isChanged || isFormEmpty || first[(site / 2)] || ww < 900) {
                createNew();
            }
            else {
                const send = await confirmClose();
                if (send) {
                    if (send[0] && send[1]) {
                        try {
                            let method = 'put';
                            if ($(`#submit${site}`).hasClass("newItem")) {
                                method = 'post';
                            }
                            const data = await sendToServer(obj, method, (site / 2));
                            successMsg(data, elem[(site / 2)]);
                            createNew();
                        }
                        catch (ex) {
                            errorPrompt(ex);
                        }
                    }
                    else if (send[0] && !send[1]) {
                        createNew();
                    }
                }
            }
        }
    });
    function createNew() {
        cleanUrl();
        $(".oL, .overLay").removeClass("active");
        if (ww < 900) $(".rightBar").addClass("active");
        $(`#title${site}`).val("");
        $(`#submit${site}`).attr("class", "newItem").text("Add item");
        if (first[(site / 2)] == true) {
            $(`.edit${site}`).addClass("active");
            $(".noItems").addClass("disabled");
        }
        first[(site / 2)] = false;
        $(`#statusMessage${site}`).html("");
        if (site == 0) {
            $("#scoreDisplay").addClass("disabled");
            $("#desc,#alt,#imgUrl,#la,#lo").val("");
            $("#hgw").prop('checked', false);
            $("#pic").replaceWith($("#pic").val('').clone(true));
            $("#imgPreview").attr("src", `../img/placeholder.png`);
            $("#imgUrl").val('placeholder.png');
        }
        else if (site == 2) {
            $("#title2").attr("placeholder", "(Optional, defaults to uploaded file name)");
            $("#extLine, #sizeLine, #usedByLine, #previewLine, #imgPreview2, .upload2").addClass("disabled");
            $("#submit2").text("Upload");
            $("#uploadBox").addClass("active").html("")
                .append("<h4>File to upload</h4><input id='pic2' type='file' name='pic2' accept='image/*'></input>");
        }
    }
    function measure() {
        const h = $(".popupPreview").height();
        const w = $(".popupPreview").width();
        const ml = w + ((window.innerWidth - w) / 2) - 20;
        $(".imagePopup .cross").css("margin-left", `${ml}px`);
        if (h > w) {
            $(".popupPreview").addClass("high");
        }
    }
    function sendToServer(obj, met, p) {
        return new Promise(async (resolve, reject) => {
            let idSet = obj._id;
            let para = "";
            let headers = { "x-auth-token": cook };
            let dest = ["locations/", "images/"];
            let w = "updated";
            if (met == 'post') {
                idSet = "";
                w = "added";
                if (p == 2) {
                    $("#progressbar2, #cU-2").addClass("active");
                    $("#progressbar2").progressbar({
                        value: false
                    });
                }
            }
            delete obj._id;
            if (p == 0) {
                delete obj.score;
            }
            let fObj;
            if (obj.file) {
                fObj = new FormData();
                let file = document.getElementById("pic2").files[0];
                fObj.append('file', file);
                delete obj.file;
                if (met == 'post' && obj.title) {
                    para = `?title=${obj.title}`;
                }
                else if (met == 'put') {
                    para = `?title=${obj.title}&ext=${obj.type}&size=${obj.size}`;
                }
            }
            else {
                if (!$("#submit2").hasClass("newItem") && site == 2 && parseInt($("#size").attr("class")) != selected[1].size) {
                    obj.temp = ($("#imgPreview2").attr("src").split("/temp/")[1]).split("?")[0];
                }
                fObj = JSON.stringify(obj);
                headers = {
                    'x-auth-token': cook,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                };
            }
            try {
                const data = await sendReq(met, para, dest[site / 2], headers, idSet, fObj);
                if (data) {
                    if (site == 0) {
                        if ($("#usedList").html().indexOf(data._id) == -1 && $("#title2").val() == (data.img.split('.')[0])) {
                            if ($("#usedList").html().indexOf("N/A") > -1) $("#usedList").html("");
                            let str = $("#usedList").html();
                            if ($("#usedList").html().length > 0) str += ", ";
                            str += `<a href='#locations?objectid=${data._id}'>${data.title}</a>`;
                            $("#usedList").html(str);
                        }
                        else if ($("#usedList").html().indexOf(data.title) > -1 && $("#title2").val() != (data.img.split('.')[0])) {
                            const str = $("#usedList").text();
                            const arrayOfObjs = updateArrayOfStrings(str, true, data.title, data.title);
                            const generatedString = checkIfUsedBy(arrayOfObjs);
                            $("#usedList").html(generatedString);
                        }
                    }
                    if (met == 'put') {
                        let generatedString = "",
                            calcSize = "",
                            inj;
                        if (site == 0) {
                            inj = `<ul><li class='objDesc'>${data.desc}</li><li class='altitude'><b>Altitude:</b> <span class='altitudeSpan'>${data.alt}</span> m</li>` +
                                `<li class='hgw'><b>Image width greater than height:</b> <span class='hgwValue'>${data.hgw}</span>` +
                                `<li><b>Image url: </b><span class='imgUrl'>${data.img}</span></li><li><b>Coordinates: Lt:` +
                                `</b> <span class='La'>${data.pos.coordinates[0]}</span>,  <b>Ln:</b> <span class='Lo'>` +
                                `${data.pos.coordinates[1]}</span></li><li><b>Average rating:</b> <span class='score'>${data.score}</span>`;
                            if ($("#usedList").text().indexOf(selected[0].title) > -1 && selected[0].title != data.title) {
                                const str = $("#usedList").text();
                                updateArrayOfStrings(str, false, selected[0].title, data.title);
                            }
                        }
                        else {
                            if ($("#imgUrl").val() == `${selected[1].title}.${selected[1].type}` && $("#imgUrl").val() != `${data.title}.${data.type}`) {
                                $("#imgUrl").val(`${data.title}.${data.type}`)
                            }
                            generatedString = checkIfUsedBy(data.usedBy);
                            calcSize = calcSizeF(data);
                            inj = `<ul><li><b>Extension:</b> <span class='ext'>${data.type}</span></li><li><b>Size:` +
                                `</b> <span class='sizeValue' data-index='${data.size}'>${calcSize} kb</span></li>` +
                                `<li><b>Used by:</b> <span class='usedByString'>${generatedString}</span></li>`;

                            caches.open('v1').then(function (cache) {
                                cache.delete(`../img/${data.title}.${data.type}`).then(function () {
                                    updateImg(`${data.title}.${data.type}`, 2, true);
                                    setNew = true;
                                });
                            })
                        }
                        $(`#${data._id}`).html(`${te}<h3>${data.title}</h3>${inj}</ul>`);
                        const i = objectList.findIndex(x => x._id === data._id);
                        objectList[i] = data;
                        updateEdit($(`#${data._id}`), data._id);
                    }
                    else {
                        objectList.push(data);
                        objectList.sort((a, b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
                        if (p == 1) $("#progressbar2, #cU-2").removeClass("active");
                        $(`#submit${p}`).attr("class", data._id);
                        let j = objectList.findIndex(x => x.title === data.title);
                        const injection = inject(data);
                        const str = $(`<div id='${data._id}' class='oL'></div><div class='overLay ${data._id}'></div>`);
                        if (j == 0) {
                            const idToGet = objectList[j + 1]._id;
                            str.insertBefore(`#${idToGet}`);
                        }
                        else {
                            const idToGet = objectList[j - 1]._id;
                            str.insertAfter(`.overLay.${idToGet}`);
                        }
                        $(`#${data._id}`).html(injection);
                    }
                    return resolve([data, w]);
                }
            }
            catch (err) {
                let c = await convertError(err);
                if (c.details) c = c.details[0].message;
                c = (c).replace(/'/g, "&apos;").replace(/"/g, "&quot;");

                if (p == 2) $("#progressbar2, #cU-2").removeClass("active");
                return reject(c);
            }
        });
    }
    return {
        measureWidth: measureWidth,
        measure: measure,
        init: init
    };
})();

admin.init();