$(document).ready(async function(e){if(document.cookie){let cookie=document.cookie;if(cookie.indexOf("token")>-1){let cook;let value="; "+cookie;let parts=value.split("; token=");if(parts.length==2)cook=parts.pop().split(";").shift();try{const res=await sendReq('get',"","login",{'x-auth-token':cook},"",null);if(res)window.location.href="../admin"}
catch(ex){$(".status").html(ex)}}
if('serviceWorker' in navigator){$(window).on("load",async()=>{try{await navigator.serviceWorker.register("../serviceworker.js")}
catch(ex){console.log(ex)}})}}});function sendReq(method,query,dest,headers,id,body){return new Promise(async(resolve,reject)=>{try{const res=await fetch(`../api/${dest}${id}${query}`,{method:method,mode:'cors',headers:headers,body:body});if(res.status>=200&&res.status<400){return resolve(res.json())}
else{throw(res)}}
catch(err){const c=await convertError(err);reject(c)}})}
function convertError(err){return new Promise((resolve,reject)=>{let extracted="";if(err.status){err.json().then(data=>{extracted=data.msg;return resolve(extracted)}).catch(ex=>{extracted=err.statusText;return resolve(extracted)})}
else{return resolve("No connection, or the operation was aborted")}})}
$(".loginField").on("submit",function(e){e.preventDefault();login()});$(".forgotPassword").on("submit",function(e){e.preventDefault();sendEmail()});$("#forgot").on("mousedown",function(e){if(e.button==0){$(".loginField").addClass("disabled");$(".forgotPassword").addClass("active")}});$("#cancel").on("mousedown",function(e){if(e.button==0){closeReset()}});function closeReset(){$(".loginField").removeClass("disabled");$(".forgotPassword").removeClass("active");$("#passwordResetEmail").val("");$(".status2").html("")}
async function login(){try{const data=await sendReq("post","","login",{'Accept':'application/json','Content-Type':'application/json'},"",JSON.stringify({email:$("#emailF").val(),password:$("#password").val()}));if(data){if(data.success==!0){let d=new Date();d.setTime(d.getTime()+(7*24*60*60*1000));const expires="expires="+d.toUTCString();document.cookie="token ="+data.jwt+";"+expires+";path=/";window.location.href="../admin"}
else{$(".status").html(data.msg)}}}
catch(err){$("#password").val('');$(".status").html(err)}}
async function sendEmail(){try{const data=await sendReq('post',"","users/resetpassword",{'Accept':'application/json','Content-Type':'application/json'},"",JSON.stringify({email:$("#passwordResetEmail").val(),}));if(data){$(".status2").html(data.msg);setTimeout(function(){closeReset()},3500)}}
catch(err){$(".status2").html(err)}}